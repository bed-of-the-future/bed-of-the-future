This folder contains all transformed data sheets constructed from the raw results sheet and the R script used for the statistical analysis. 

Document explanations:

- TMR_for_R: Raw results sheet with abbreviated column headings and text answers removed
- All_All, All_G1, All_G2: All total score results in one column with their respective time point and group the scores come from (_All) as well as group split. Necessary transformation for ANOVA calculation and model construction
- S_All, S_G1, S_G2: All scented score results in one column with their respective time point and group the scores come from (_All) as well as group split. Necessary transformation for ANOVA calculation and model construction
- US_All, US_G1, US_G2: All unscented score results in one column with their respective time point and group the scores come from (_All) as well as group split. Necessary transformation for ANOVA calculation and model construction
- All_All_with_SleepQ_NLang, S_All_with_SleepQ_NLang, US_All_with_SleepQ_NLang: All_All, S_All and US_All with the addition of values for Number of languages known and sleep quality - necessary for the ANOVAs calculating the influence of number of languages known and sleep quality on learning rate

TMR Study Repository

This repository contains the raw result sheet of the TMR study and a subfolder (R) containing everything related to the statistical analysis in R (transformed data & R Script). In addition, it hosts the study report as a standalone PDF.

Raw Results Sheet

The raw results sheet includes the following columns:

- Reasons to exclude: Participants (Ps) were given the chance to comment on interferences with the study paradigm. Two Ps described significant non-adherence to the study paradigm in these comment sections. Here, these Ps are marked, and their problems shortly described. The two marked Ps were excluded from the statistical analysis.
- Group: Group of the respective P (1 = 4h scent, 2 = whole night scent)
- Pcode: Pcode used to track the Ps throughout the surveys (one P did not adhere to the Pcode convention (14) - luckily they stayed coherent with this error throughout all surveys so the wrong Pcode could still be used for tracking)
- Age: Age of the respective P
- Gender: Gender of the respective P
- Primary Language: Mother tongue / primary language of the respective P
- Number Languages: Number of languages known by the respective P - 6 represents the answer "more than 5 languages known"
- Score: Number of correctly remembered total words for test timepoint 1 (Learning 1), 2 (Learning 2), 3 (Learning 3), and 4 (Final Test) for the respective P
- Score scented: Number of correctly remembered scented words for test timepoint 1 (Learning 1), 2 (Learning 2), 3 (Learning 3), and 4 (Final Test) for the respective P
- Score unscented: Number of correctly remembered unscented words for test timepoint 1 (Learning 1), 2 (Learning 2), 3 (Learning 3), and 4 (Final Test) for the respective P
- Interference Learning: Answer to the question: "Is there anything that could have interfered with the learning or testing period? For example: Did the scent not develop correctly? Was the scent still present in the unscented parts? Did someone or something interrupt you?" for test timepoint 1 (Learning 1), 2 (Learning 2), 3 (Learning 3), and 4 (Final Test) for the respective P
- Final Remarks: Answer to the question: "Thank you so much for your participation! Are there any final remarks or comments you want to share with us on the study or your experience with it?" for test timepoint 1 (Learning 1), 2 (Learning 2), 3 (Learning 3), and 4 (Final Test) for the respective P
- Stick to timeslot: Answer to the question: "Did you stick to the time slot you usually use for the learning periods?" for test timepoint 2 (Learning 2), 3 (Learning 3), and 4 (Final Test) for the respective P
- Score sleep quality: Likert-scale (Bad, Rather Bad, Neutral, Rather Good, Good) answer to the question: "Please rate your sleep quality for today's night" for test timepoint 2 (Learning 2), 3 (Learning 3), and 4 (Final Test) for the respective P
- Transformed score sleep quality: Likert-Scale answers transformed into numbers
- Comment sleep quality: Answer to the question: "Give a short description of your sleeping experience. Did scent or diffuser interfere with your sleep? Or was the scent pleasant and soothed you to sleep?" for test timepoint 2 (Learning 2), 3 (Learning 3), and 4 (Final Test) for the respective P
- Dream Recall: Answer to the question: "Do you recall that the learning material or anything related to the study appearing in your dreams? If so, please write down your dream experience." for test timepoint 2 (Learning 2), 3 (Learning 3), and 4 (Final Test) for the respective P


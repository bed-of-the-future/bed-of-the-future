import numpy as np
import pywt
from scipy.signal import butter, lfilter
import neurokit2 as nk

class SignalCleaner:
    def __init__(self, fs=500, low_pass_order=5, wavelet="db4", wavelet_level=6, neurokit_method="neurokit"):
        self.fs = fs
        self.low_pass_order = low_pass_order
        self.wavelet = wavelet
        self.wavelet_level = wavelet_level
        self.neurokit_method = neurokit_method

    def butter_lowpass(self, cutoff):
        nyquist = 0.5 * self.fs
        normal_cutoff = cutoff / nyquist
        b, a = butter(self.low_pass_order, normal_cutoff, btype='low', analog=False)
        return b, a

    def butter_lowpass_filter(self, data, cutoff):
        b, a = self.butter_lowpass(cutoff)
        y = lfilter(b, a, data)
        return y

    def wavelet_denoise_with_freq_range(self, data, freq_range=(0, 10)):
        coeffs = pywt.wavedec(data, self.wavelet, level=self.wavelet_level)
        threshold = np.median(np.abs(coeffs[-1])) / 0.6745

        for i in range(1, len(coeffs)):
            freq_min = 0.5 * self.fs / (2 ** i)
            freq_max = 10 * self.fs / (2 ** i)

            if freq_min > freq_range[1] or freq_max < freq_range[0]:
                continue

            coeffs[i] = pywt.threshold(coeffs[i], threshold)
        denoised_signal = pywt.waverec(coeffs, self.wavelet)
        return denoised_signal

    def neurokit2_filter(self, data):
        signals, info = nk.ecg_process(data, sampling_rate=500, method=self.neurokit_method)

        # get the cleaned signal
        cleaned_signal = signals["ECG_Clean"]

        return cleaned_signal

# Example usage:
if __name__ == '__main__':
    signal_processor = SignalCleaner(fs=500, low_pass_order=6, wavelet="db4", wavelet_level=6, neurokit_method="neurokit")
    cutoff = 10  # Specify the cutoff frequency
    freq_range = (0,10) # Specify the frequency range in Hz

    data = np.load("some_data")

    # Example 1: Butterworth low-pass filter
    b, a = signal_processor.butter_lowpass(cutoff=cutoff)
    filtered_data = signal_processor.butter_lowpass_filter(data, cutoff=cutoff)

    # Example 2: Wavelet denoising
    denoised_signal = signal_processor.wavelet_denoise_with_freq_range(data, freq_range=freq_range)

    # Example 3: neurokit filter
    neurokit_filtered_signal = signal_processor.neurokit2_filter(data)
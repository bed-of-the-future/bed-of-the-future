import matplotlib.pyplot as plt
import matplotlib.animation as animation
from matplotlib.widgets import Slider
import time
import busio
import adafruit_ads1x15.ads1115 as ADS
from adafruit_ads1x15.analog_in import AnalogIn
from adafruit_ads1x15.ads1x15 import Mode


"""
Here, the I2C bus(es) are created. The address may needs to be changed. 0x48 is the default one. 
If two ads1115 are used and connected to the Raspi in parallel, the defaults are 0x48 and 0x49.
To achieve the frequency specified here, the I2C clock rate of the Raspi must be set to fast mode(400 kbit/s).
"""
# Create the I2C buses
i2c1 = busio.I2C(3, 2, frequency=400000)
ads1 = ADS.ADS1115(i2c1, address=0x48)
i2c2 = busio.I2C(3, 2, frequency=400000)
ads2 = ADS.ADS1115(i2c2, address=0x49)

# Specify the gain that should be used
GAINs = []
print("Which gain should be used? 1 or 2 or 4 or 8 or 16.")
print("NOTE: Be cautious, the higher the gain, the small the input voltage must be to not break the ADS ")
for i in range(2):
    GAINs.append(int(input("Gain for ads "+str(i)+": ")))

"""
Here, we set the ads1115 settings, the mode must be continuous to achieve a high sample rate.
The data rate is set to the max value. This rate will not be achieved. In our tests, we reached ~700 sps.

"""
ads1.mode = Mode.CONTINUOUS
ads1.data_rate = 860
ads1.gain = GAINs[0]

ads2.mode = Mode.CONTINUOUS
ads2.data_rate = 860
ads2.gain = GAINs[1]

# specify the ads input channels (input pins), we usually use channel 0.
chen0 = AnalogIn(ads1, ADS.P0)
chen1 = AnalogIn(ads2, ADS.P0)

# Everything from here has to do with making the Graph and have it Auto-Update
# Number of points to display along X axis, make this larger to show more Points.
x_len = 500

# Range of possible Y values to display, when using the gain value 16 this is a good maximum y_range for subtle movement.
# You can also adjust the viewing size of the graph in the live window
y_1 = chen0.value 
y_2 = chen1.value

y_range_1 = [y_1-1000, y_1+1000]
y_range_2 = [y_2-1000, y_2+1000]
# Create figure for plotting the LIVE Plot graph and some important variables
fig = plt.figure(figsize=(9,7))

# Create subplot for the "value" variable
ax1 = fig.add_subplot(2, 1, 1)
xs = list(range(0, x_len))
ys = [0] * x_len
ax1.set_ylim(y_range_1)
line1, = ax1.plot(xs, ys)

# Create subplot for the "numb" variable
ax2 = fig.add_subplot(2, 1, 2)
xs2 = list(range(0, x_len))
ys2 = [0] * x_len
ax2.set_ylim(y_range_2)
line2, = ax2.plot(xs2, ys2)

# Add labels to the subplots
ax1.set_title('PPG Sensor')
ax1.set_xlabel('Data Points (Approximately 25 Readings each Second)')
ax1.set_ylabel('Converted 16 bits number,  Post Gain Adjustment')

ax2.set_title('Geophone')
ax2.set_xlabel('Data Points (Approximately 25 Readings each Second)')
ax2.set_ylabel('Converted 16 bits number, Post Gain Adjustment')

"""
# Rest of the code remains the same...
ax_slider1 = plt.axes([0.25, 0.04, 0.65, 0.02])
slider1 = Slider(ax_slider1, 'Y-axis Range (Value)', 0, 30000, valinit=5000)

ax_slider2 = plt.axes([0.25, 0.01, 0.65, 0.02])
slider2 = Slider(ax_slider2, 'Y-axis Range (Numb)', 0, 30000, valinit=5000)
"""
# Create sliders for adjusting the y-axis bounds of ax1 and ax2
ax_slider_1_upper = plt.axes([0.25, 0.04, 0.65, 0.02])
slider_1_upper = Slider(ax_slider_1_upper, 'Y-axis Upper Bound (Value)', 0, 30000, valinit=1000)

ax_slider_1_lower = plt.axes([0.25, 0.02, 0.65, 0.02])
slider_1_lower = Slider(ax_slider_1_lower, 'Y-axis Lower Bound (Value)', 0, 30000, valinit=-1000)

ax_slider_2_upper = plt.axes([0.25, 0.6, 0.65, 0.02])
slider_2_upper = Slider(ax_slider_2_upper, 'Y-axis Upper Bound (Numb)', 0, 30000, valinit=1000)

ax_slider_2_lower = plt.axes([0.25, 0.58, 0.65, 0.02])
slider_2_lower = Slider(ax_slider_2_lower, 'Y-axis Lower Bound (Numb)', 0, 30000, valinit=-1000)



def update_sliders_1(d):
   # y_range1 = [0, slider1.val]
    
    y_upper_1 = slider_1_upper.val
    y_lower_1 = slider_1_lower.val    
    ax2.set_ylim(y_lower_1,y_upper_1)    #(y_range1)

 #   y_range2 = [0, slider2.val]
 #   ax2.set_ylim(y_range2)
    plt.draw()

def update_sliders_2(d):
  #  y_range2 = [0, slider2.val]
    y_upper_2 = slider_2_upper.val
    y_lower_2 = slider_2_lower.val
    ax1.set_ylim(y_lower_2,y_upper_2)  #(y_range2)
    plt.draw()




# Attach slider update functions to the sliders
slider_1_upper.on_changed(update_sliders_1)
slider_1_lower.on_changed(update_sliders_1)
slider_2_upper.on_changed(update_sliders_2)
slider_2_lower.on_changed(update_sliders_2)


"""
slider1.on_changed(update_sliders_1)
slider2.on_changed(update_sliders_2)
"""
def animate(i, ys, ys2):

    value1 = chen0.value
    value2 = chen1.value
    ys.append(value1)
    ys = ys[-x_len:]
    line1.set_ydata(ys)

    ys2.append(value2)
    ys2 = ys2[-x_len:]
    line2.set_ydata(ys2)
#    time.sleep(1.0)
    return line1, line2


# Set up plot to call animate() function periodically
ani = animation.FuncAnimation(fig,
                              animate,
                              fargs=(ys, ys2),
                              interval=1,
                              blit=True)
plt.show()

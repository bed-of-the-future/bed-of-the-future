import board
import busio
import adafruit_ads1x15.ads1115 as ADS
from adafruit_ads1x15.analog_in import AnalogIn
from adafruit_ads1x15.ads1x15 import Mode
import influxdb_client
from influxdb_client import  Point
from influxdb_client.client.write_api import SYNCHRONOUS, ASYNCHRONOUS
import os
# the following are only required for dummy data generation
#import random
#import numpy as np


def check_for_stop_signal():
	"""
	This method checks if there is a .txt file created. If it is,
	the script will stop recording data
	"""
    if os.path.exists("/home/raspberry/Desktop/stop_recording_sensors.txt"):
        return True
    else:
        return False
try:
    os.remove("/home/raspberry/Desktop/stop_recording_sensors.txt")
except FileNotFoundError:
    pass
    
# InfluxDB connection parameters
url = "http://localhost:8086"
token = 'rqU6HxUnS6RrYfthKGjBdIqkbe9Us8DdJSnm6K8IIz5mm74I8FBKtD25uhTiLAAfafyOBg8agC3sqsmSRyKUeQ=='
org = "smartbed"  
bucket = "RawData"  

# Create the I2C buses for the 2 ADS
i2c = busio.I2C(board.SCL, board.SDA, frequency=400000)
ads1 = ADS.ADS1115(i2c, address=0x48)
ads2 = ADS.ADS1115(i2c, address=0x49)

# Here, we set the ads1115 settings
ads1.mode = Mode.CONTINUOUS
ads1.data_rate = 128
ads1.gain = 1

# Here, we set the ads1115 settings
ads2.mode = Mode.CONTINUOUS
ads2.data_rate = 128
ads2.gain = 1

# Specify the ads input channels (input pins), we usually use channel 0
chen0 = AnalogIn(ads1, ADS.P0)
chen1 = AnalogIn(ads2, ADS.P0)

# Set up client and api to write data to influx
write_client = influxdb_client.InfluxDBClient(url=url, token=token, org=org)
write_api = write_client.write_api(write_options=SYNCHRONOUS)

# Record data until .txt filé is created
while True:
	# check if .txt is created
    condition =  check_for_stop_signal()
    if condition == True:
        break

    """
    DUMMY DATA GENERATION (testing purposes, only for ONE ads/channel)
    if x < 50:
        values["chen0"]=((2 *x)*x) + random.randint(0,10000)
    else:
        values["chen0"] = (2*x)*x - (x*x) + random.randint(0,10000)
    if x == 100:
        x = 0
    print("Value1:", values["chen0"]) 
    """
    # get point from ads1 (in our case the geophone) and create point object for influx
    point_geo = (
        Point("RawDataGeophone")
        .field("rawData",int(chen0.value))
    )
    # write point to influx
    write_api.write(bucket=bucket, org=org, record=point_geo, write_options=SYNCHRONOUS)

    # get point from ads2 (in our case the piezo) and create point object for influx
    point_piezo = (
        Point("RawDataPiezo")
        .field("rawData",int(chen1.value))
        )
    # write point to influx
    write_api.write(bucket=bucket, org=org, record=point_piezo, write_options=SYNCHRONOUS)

# remove the stop txt fo that the script can be run again
os.remove("stop_recording_sensors.txt")

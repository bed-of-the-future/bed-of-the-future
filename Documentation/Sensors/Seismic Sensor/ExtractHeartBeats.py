from datetime import datetime, timedelta
from influxdb_client import InfluxDBClient
import influxdb_client, os, time
from influxdb_client import InfluxDBClient, Point, WritePrecision
from influxdb_client.client.write_api import SYNCHRONOUS
import neurokit2 as nk

# Influx Login 
# InfluxDB connection parameters
url = "http://localhost:8086"
token = 'rqU6HxUnS6RrYfthKGjBdIqkbe9Us8DdJSnm6K8IIz5mm74I8FBKtD25uhTiLAAfafyOBg8agC3sqsmSRyKUeQ=='
org = "smartbed"  # Replace with your InfluxDB organization
bucket = "RawData"  # Replace with your InfluxDB bucket

# Set up client and APIs
client = influxdb_client.InfluxDBClient(url=url, token=token, org=org)

# Initialize the InfluxDB client
query_api = client.query_api()
write_api = client.write_api(write_options=SYNCHRONOUS)

# 1st Query, get the last 20s of raw data
query = """from(bucket: "RawData")
 |> range(start: -20s)
 |> filter(fn: (r) => r._measurement == "RawDataGeophone")"""
tables = query_api.query(query, org=org)

# extract the values from the querie's result
time_values = []
values = []

# Iterate through the results and store _time values
for table in tables:
    for record in table.records:
        time_values.append(record['_time'])
        values.append(record["_value"])
# compute the sample rate, i.e. count of datapoints / 60s
sample_rate = len(values)/20

# Clean the raw data
signals = nk.ecg_clean(values, sampling_rate=sample_rate)

# Extract the heart beats
rpeaks = nk.ecg_findpeaks(signals, sampling_rate=sample_rate)

# write the heart beats to influx
for heartbeat in rpeaks["ECG_R_Peaks"]:
    point = (
        Point("sensor_geophone")
        .field("heartBeat",1)
        .time(time_values[heartbeat])
    )
    write_api.write(bucket="Data", org=org, record=point)

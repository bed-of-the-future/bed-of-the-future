# Seismic Sensor for vital sign monitoring

This repository contains all code used for the seismic sensor. For an in-depth explanation of the general architecture of the sensor, see: https://gitlab.gwdg.de/bed-of-the-future/sensors/seismic/-/wikis/General%20Idea%20and%20Overview

## Code
The code in this folder can only be used together with an InfluxDB instance. The code is created for the Raspberry Pi and the OS Raspbian, other devices have not been tested. The Raspberry Pi must have I2C communication enabled. Given the requirements.txt, an appropiate Python virtual environment must be created, which must be set withhin the final Node-Red flow. Finally, the scripts must be configured, i.e. the correct I2Cports, the correct InfluxDB configurations and the correct path for the "stop-recording" file in the script "influxWriteData.py", must be set. 

For the scripts to run the seismic sensor without Influx, see: https://gitlab.gwdg.de/bed-of-the-future/sensors/seismic/-/tree/main

## Integration
The integration of the scripts here can be found in the final Node-Red flow in GUI folder (https://gitlab.gwdg.de/bed-of-the-future/bed-of-the-future/-/blob/main/Documentation/GUI/flows-29.json). The flow contains additional comments and explanations.


## Additional
The folder additional contains scripts not necessary for the smart bed to work, but which might help future users. 

- Plot2Sensors.py can be used to create an interactive plot of the signals recieved from 2 ADS1115. 
- SignalCleaner.py is a class with three different signal cleaning algorithms implemented

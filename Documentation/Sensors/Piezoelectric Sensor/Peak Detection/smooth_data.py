import numpy as np
from scipy.signal import hilbert


def smooth_data(data, window_size):
    analytic_signal = hilbert(data)
    env = np.abs(analytic_signal)
    moving_averages = []
    for i in range(len(data) - window_size + 1):
        window = env[i:i + window_size]
        average = np.mean(window)
        moving_averages.append(average)
    return moving_averages


# Example usage:
butterworth_data = [...]  # Your data here
window_size = 70
result = smooth_data(butterworth_data, window_size)
print(result)

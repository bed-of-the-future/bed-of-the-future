from scipy.signal import butter, lfilter


def butter_bandpass_filter(data, lowcut, highcut, sampling_rate, order=2):
    """
    Apply a Butterworth bandpass filter to the input data.

    Parameters:
        data (array-like): Input data to be filtered.
        lowcut (float): Lower cutoff frequency of the filter.
        highcut (float): Upper cutoff frequency of the filter.
        sampling_rate (float): Sampling rate of the input data.
        order (int): Order of the filter (default is 2 for respiration).

    Returns:
        filtered_data (array): Filtered data.
    """
    nyquist = 0.5 * sampling_rate
    low = lowcut / nyquist
    high = highcut / nyquist

    # Get Butterworth filter coefficients
    b, a = butter(order, [low, high], btype='band')

    # Apply the filter to the data
    filtered_data = lfilter(b, a, data)

    return filtered_data

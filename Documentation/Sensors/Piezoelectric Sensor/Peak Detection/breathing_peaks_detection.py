import numpy as np
import matplotlib.pyplot as plt
from scipy.signal import argrelextrema
from scipy.signal import find_peaks


def detect_and_plot_respiration_peaks(data, sampling_rate, order=400):
    """
    Detect peaks in the input data and plot the data with detected peaks.

    Parameters:
        data (array-like): Input data for peak detection and plotting(Use filtered data after butterworth filter).
        sampling_rate (float): Sampling rate of the input data.
        order (int): Order of the peak detection algorithm (default is 400).

    Returns:
        fig (matplotlib.figure.Figure): The plot figure.
        max_values (numpy.ndarray): Indices of the detected peaks.
    """
    # Find peak indices using argrelextrema
    max_values = np.array(argrelextrema(data, np.greater, order=order))

    # Find Peak indices using find_peaks
    # peaks, _ = find_peaks(data, height=0, distance=180)

    # Convert peak indices to time
    time = np.arange(len(data)) / sampling_rate
    peak_times = [peak_index / sampling_rate for peak_index in max_values]

    # Plot data and detected peaks
    fig, ax = plt.subplots(figsize=(8, 2))
    for peak_index in max_values:
        ax.vlines(peak_times, ymin=-0.2, ymax=0.2,
                  linestyle='--', color='orchid')

    ax.plot(time, data, color='dodgerblue')

    # Add labels and legend
    ax.set_xlabel('Time')
    ax.set_ylabel('Amplitude')
    ax.set_xlim(16, 36)
    ax.set_xticks(np.arange(16, 37, step=2))
    ax.text(26, -0.4, '(b)', fontsize=11, fontweight='bold', ha='center')

    # Show plot
    plt.show()

    return fig, max_values

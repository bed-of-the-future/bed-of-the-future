
import numpy as np
from scipy.signal import butter, lfilter


def find_heartbeat(data, lowf=2, highf=10, order=4, interval_seconds_piezo=0.78):
    # Calculate sampling rate
    sampling_rate = len(data) / 20

    # Since we call the data from the database for every twenty seconds,
    # to calculate the sampling rate, we divide the number of data by twenty.
    # You can change this interval according to your needs.

    # Filter the data using Butterworth filter
    nyquist = 0.5 * sampling_rate
    low = lowf / nyquist
    high = highf / nyquist
    b, a = butter(order, [low, high], btype='band')
    butterworth_data = lfilter(b, a, data)

    # Define the desired interval in samples
    interval_samples_butterworth = int(interval_seconds_piezo * sampling_rate)

    # Find peaks using argmax for each segment
    peaks_butterworth = []
    for i in range(0, len(butterworth_data), interval_samples_butterworth):
        interval_end_piezo = min(
            i + interval_samples_butterworth, len(butterworth_data))
        peak_index_piezo = np.argmax(butterworth_data[i:interval_end_piezo])
        peaks_butterworth.append(peak_index_piezo + i)

    return peaks_butterworth

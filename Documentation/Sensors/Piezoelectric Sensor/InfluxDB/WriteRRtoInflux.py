from influxdb_client import Point
from influxdb_client.client.write_api import SYNCHRONOUS

# InfluxDB connection parameters
url = "http://localhost:8086"
token = 'rqU6HxUnS6RrYfthKGjBdIqkbe9Us8DdJSnm6K8IIz5mm74I8FBKtD25uhTiLAAfafyOBg8agC3sqsmSRyKUeQ=='
org = "smartbed"  # Replace with your InfluxDB organization
retrieve_bucket = "Data"

client = influxdb_client.InfluxDBClient(url=url, token=token, org=org)
# Initialize the InfluxDB client
query_api = client.query_api()
write_api = client.write_api(write_options=SYNCHRONOUS)

query = """from(bucket: "Data")
  |> range(start:-120s, stop: now())
  |> filter(fn: (r) => r["_measurement"] == "piezo")
  |> keep(columns: ["_time"])
  |> last(column: "_time")"""


data_breathing_time = query_api.query(query, org=org)
for table in data_breathing_time :
    for record in table.records:
        time_values = record['_time']



unix = int(time_values.timestamp())


query2 = """from(bucket: "Data")
  |> range(start: v.timeRangeStart - 120, stop: v.timeRangeStop)
  |> filter(fn: (r) => r["_measurement"] == "piezo")
  |> filter(fn: (r) => r["_field"] == "Breath")
  |> count()
  |> yield(name: "count")"""



query2 = query2.replace("v.timeRangeStart", f'{unix}')
query2 = query2.replace("v.timeRangeStop", f'{unix}')


data_breathing = query_api.query(query2, org=org)
for table in data_breathing:
    for record in table.records:
        bpm = record["_value"]
bpm = bpm/2

point = (
    Point("piezo")
    .field("Respiratory_Rate", float(bpm))

)

write_api.write(bucket="Data", org=org, record=point)


from influxdb_client import Point
from influxdb_client.client.write_api import SYNCHRONOUS
from heartbeat_influx import find_heartbeat


# InfluxDB connection parameters
url = "http://localhost:8086"
token = 'rqU6HxUnS6RrYfthKGjBdIqkbe9Us8DdJSnm6K8IIz5mm74I8FBKtD25uhTiLAAfafyOBg8agC3sqsmSRyKUeQ==' # Replace with your token
org = "smartbed"  # Replace with your InfluxDB organization
bucket = "RawData"  # Replace with your InfluxDB bucket

# Set up client and APIs
client = influxdb_client.InfluxDBClient(url=url, token=token, org=org)

# Initialize the InfluxDB client
query_api = client.query_api()
write_api = client.write_api(write_options=SYNCHRONOUS)

# 1st Query, get the last 60s of raw data
query = """from(bucket: "RawData")
 |> range(start: -20s)
 |> filter(fn: (r) => r._measurement == "RawDataPiezo")"""
tables = query_api.query(query, org=org)

# extract the values from the querie's result
time_values = []
values = []

# Iterate through the results and store _time values
for table in tables:
    for record in table.records:
        time_values.append(record['_time'])
        values.append(record["_value"])
# print(len(values))

############################################### Heartbeat Peak Detection #########################################################
peaks = find_heartbeat(values, lowf=2, highf=10, order=4, interval_seconds_piezo=0.78)

# write the heart beats to influx
for heartbeat in peaks:
    point = (
        Point("piezo")
        .field("HeartBeat",1)
        .time(time_values[heartbeat])
    )
    write_api.write(bucket="Data", org=org, record=point)

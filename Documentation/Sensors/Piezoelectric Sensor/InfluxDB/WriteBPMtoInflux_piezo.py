
from influxdb_client import Point
from influxdb_client.client.write_api import SYNCHRONOUS

# InfluxDB connection parameters
url = "http://localhost:8086"
token = 'rqU6HxUnS6RrYfthKGjBdIqkbe9Us8DdJSnm6K8IIz5mm74I8FBKtD25uhTiLAAfafyOBg8agC3sqsmSRyKUeQ=='
org = "smartbed"  # Replace with your InfluxDB organization
retrieve_bucket = "Data"

# Initialize client and APIs
client = influxdb_client.InfluxDBClient(url=url, token=token, org=org)

# Initialize the InfluxDB client
query_api = client.query_api()
write_api = client.write_api(write_options=SYNCHRONOUS)

# 1st query, get the last Heart Beat written to influx 
query = """from(bucket: "Data")
  |> range(start:0, stop: now())
  |> filter(fn: (r) => r["_measurement"] == "piezo")
  |> keep(columns: ["_time"])
  |> last(column: "_time")"""

last_beat = query_api.query(query, org=org)

# Get the timestamp from the last heart beat
for table in last_beat:
    for record in table.records:
        last_beat_time_value = record['_time']

# Convert the timestamp to unix time format
last_beat_time_value_unix = int(last_beat_time_value.timestamp())

# 2nd  query, count the heart beats of the last 60s, starting from the last heart beat
query2 = """from(bucket: "Data")
  |> range(start: v.timeRangeStart - 60, stop: v.timeRangeStop)
  |> filter(fn: (r) => r["_measurement"] == "piezo")
  |> filter(fn: (r) => r["_field"] == "HeartBeat")
  |> count()
  |> yield(name: "count")"""

# replace time with the actual timestamp of the last heart beat  
query2 = query2.replace("v.timeRangeStart",f'{last_beat_time_value_unix}')
query2 = query2.replace("v.timeRangeStop",f'{last_beat_time_value_unix}')

# execute the query
heart_beat_count = query_api.query(query2, org=org)

# extract the bpm value
for table in heart_beat_count:
    for record in table.records:
        bpm = int(record["_value"])        

# write the bpm value to influx
point = (
    Point("piezo")
    .field("Heart_Rate_piezo", int(bpm))
)

write_api.write(bucket="Data", org=org, record=point)

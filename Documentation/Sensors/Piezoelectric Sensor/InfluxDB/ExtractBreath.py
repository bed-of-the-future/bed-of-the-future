
from influxdb_client import Point
from influxdb_client.client.write_api import SYNCHRONOUS
import neurokit2 as nk



# InfluxDB connection parameters
url = "http://localhost:8086"
token = 'rqU6HxUnS6RrYfthKGjBdIqkbe9Us8DdJSnm6K8IIz5mm74I8FBKtD25uhTiLAAfafyOBg8agC3sqsmSRyKUeQ==' # Replace with your token
org = "smartbed"  # Replace with your InfluxDB organization
bucket = "RawData"  # Replace with your InfluxDB bucket


client = influxdb_client.InfluxDBClient(url=url, token=token, org=org)
# Initialize the InfluxDB client
query_api = client.query_api()
write_api = client.write_api(write_options=SYNCHRONOUS)

query = """from(bucket: "RawData")
 |> range(start: -60s)
 |> filter(fn: (r) => r._measurement == "RawDataPiezo")
 |> filter(fn: (r) => r._field == "rawData")"""
 

tables = query_api.query(query, org=org)

times = []
values = []
# Iterate through the results and store _time values
for table in tables:
    for record in table.records:
        times.append(record['_time'])
        values.append(record["_value"])
sample_rate = len(values)/60

############################################### Respiration Peak Detection #########################################################

### Neurokit2 ###
breath_signals = nk.rsp_clean(values, sampling_rate=sample_rate)
breath_peaks, peaks_dict = nk.rsp_peaks(breath_signals, sampling_rate=sample_rate)

############################################## Write respiratory peaks into influxdb################################################ 
for breath in range(len(breath_peaks["RSP_Peaks"])):
     if breath_peaks["RSP_Peaks"][breath]:
          print(breath)
          point = (
               Point("piezo")
               .field("Breath", 1)
               .time(times[breath])
          )
          
          write_api.write(bucket="Data", org=org, record=point)


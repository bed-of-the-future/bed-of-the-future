# **Piezoelectric Sensor for vital sign monitoring**

In this project, we used a [piezoelectric sensor](https://www.te.com/de/product-10184000-01.html) manufactured by TE connectivity. 
You can find the circuit diagram illustrates the setup we employed to obtain vital sign signals [here](https://gitlab.gwdg.de/bed-of-the-future/bed-of-the-future/-/blob/main/Documentation/Sensors/Piezoelectric%20Sensor/Circuit.png). It functions as a unity gain buffer with a T-network design, incorporating an AD822 amplifier.


All the codes employed for the piezoelectric sensor is included in this repository. 

These steps outline the process needed to analyze the piezo signals for both respiration and heartbeat detection:

1. **Preprocessing:**

- Apply a 2nd-order Butterworth bandpass filter (0.1Hz-1Hz) to filter raw piezo signals for detecting breathing signals.
- Apply a 4th-order Butterworth bandpass filter (1Hz-10Hz) to remove high-frequency noise for accurate detection of heartbeat peaks.

2. **Respiration Detection:**    
For detectiong the respiration peak (End of the Inhalation) you can apply three approaches:
- Utilize functions to identify all local maxima by comparing neighboring values (using scipy.signal.find_peaks, scipy.signal.argrelextrema).
- Employ fast Fourier transform (FFT) to determine the dominant frequency within the signal.
Calculate the respiratory rate by multiplying the dominant frequency by 60.
- Utilize the neurokit2 package 

3. **Heartbeat Detection:**

For detectiong the respiration peak (End of the Inhalation) you can apply three approaches:
- Calculate the heart rate by analyzing FFT results:
   - Obtain the envelope using the Hilbert function to compute the magnitude of the analytic signal.
Smooth the data using a moving average filter.
Implement FFT on the smoothed data to determine the dominant frequency, then calculate the heart rate by multiplying the frequency by 60.    
- Count the heartbeat peaks.
    - Find local maxima to locate the peaks on smoothed data and then count the number of peaks per minute.
    - Identify the max peak value in each 780ms time interval of filtered data after the Butterworth filter.


# **InfluxDB**   
You can find all the relevant codes for writing data into InfluxDB. This includes writing raw data, heartbeat, and respiration peaks, as well as heart rate and respiratory rate data.

# **Peak Detection** 
Within this directory, you will discover the functions for identifying peaks in both heartbeat and breathing.

# **FFT**
Here you will find a function for performing the Fast Fourier Transform, along with functions for plotting the FFT of the piezoelectric sensor data compared to the ground truth (PSG).

# **Raspberry Pi**
In this directory, you can find Python scripts for recording data using ADS1115 and Raspberry Pi 4.




# import required packages

import time
import board
import busio
import adafruit_ads1x15.ads1115 as ADS
from adafruit_ads1x15.analog_in import AnalogIn
from adafruit_ads1x15.ads1x15 import Mode
import numpy as np


# Create the I2C bus
i2c = busio.I2C(board.SCL, board.SDA, frequency=400000)
# board.SCL = 3
# board.SDA = 2
# i2c1 = busio.I2C(3, 2, frequency=400000)

#Create the ADC instance using the I2C bus
ads = ADS.ADS1115(i2c, address=0x48)


# to achieve 860Hz, we need to use continuous mode.
ads.mode = Mode.CONTINUOUS
ads.gain = 1
ads.data_rate = 250

# Create single-ended input on channel 0
chan = AnalogIn(ads, ADS.P0)

# Create differential input between channel 0 and 1
#chan = AnalogIn(ads, ADS.P0, ADS.P1)

# Duration of collecting data in seconds
duration = 7200

save_file_name ="piezo" 
save_path = "/home/raspberry/Desktop/Sensor" # input("Path to where the data shall be saved (for example: /home/results ) ")

save_path_file = save_path + "/" + save_file_name

values = []
voltages = []
timestamps = []
t0 = time.time()
t_end = time.time() + duration 
print("start time of recording (in sec):", time.time())
while (time.time() < t_end):
    timestamps.append(time.time())
    values.append(chan.value)
    voltages.append(chan.voltage)
    time.sleep(0.00299)

print("end time of recording (in sec):",time.time())
print(len(values))
values = np.array(values)
voltages = np.array(voltages)
timestamps = np.array(timestamps)

np.save(save_path_file + "_values.npy", values)
np.save(save_path_file + "_voltages.npy", voltages)
np.save(save_path_file + "_timestamps.npy", timestamps)

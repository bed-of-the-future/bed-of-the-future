
import numpy as np
import matplotlib.pyplot as plt
from scipy.fft import fft, fftfreq


def fft(data, sampling_rate, min_freq=0.1, max_freq=0.5):
    """
    Perform Fast Fourier Transform (FFT) on the smoothed data for heartbeat & respiration signal.

    Parameters:
        data (array-like): Input data for FFT.
        sampling_rate (float): Sampling rate of the input data.
        min_freq (float): Minimum frequency threshold for peak detection (default is 0.1 for breathing).
        max_freq (float): Maximum frequency threshold for peak detection (default is 0.5 for breathing).

    Returns:
        xf (array): Frequencies corresponding to the FFT result.
        yf (array): FFT result (complex values).
        top_frequency (float): Frequency of the top peak.
        top_magnitude (float): Magnitude of the top peak.
        top_index (int): Index of the top peak.
        N (int): Length of the input data.
    """
    # Assign input data to y
    y = data

    # Length of the input data
    N = len(y)

    # Sampling period
    T = 1.0 / sampling_rate

    # Perform FFT
    yf = fft(y)

    # Frequencies corresponding to the FFT result
    xf = fftfreq(N, T)[:N//2]

    # Filter indices based on frequency range
    valid_indices = np.where((xf > min_freq) & (xf < max_freq))[0]

    # Sort valid indices by their corresponding magnitudes
    sorted_valid_indices = sorted(
        valid_indices, key=lambda x: np.abs(yf[x]), reverse=True)

    # Select the index corresponding to the highest peak
    top_index = sorted_valid_indices[0]

    # Frequency and magnitude of the top peak
    top_frequency = xf[top_index]
    top_magnitude = np.abs(yf[top_index])

    return xf, yf, top_frequency, top_magnitude, top_index, N


def plot_fft_heartbeat(xf_piezo, yf_piezo, xf_psg, yf_psg, N_piezo, N_psg, top_index_piezo, top_frequency_piezo, top_magnitude_piezo, top_index_psg, top_frequency_psg, top_magnitude_psg):
    """
    Function to plot the FFT result with peaks for piezoelectric sensor data and ground truth data.

    Parameters:
        xf_piezo (array): Frequencies for piezoelectric sensor FFT.
        yf_piezo (array): Magnitudes for piezoelectric sensor FFT.
        xf_psg (array): Frequencies for ground truth FFT.
        yf_psg (array): Magnitudes for ground truth FFT.
        N_piezo (int): Length of piezoelectric sensor data.
        N_psg (int): Length of ground truth data.
        top_index_piezo (int): Index of the top peak for piezoelectric sensor data.
        top_frequency_piezo (float): Frequency of the top peak for piezoelectric sensor data.
        top_magnitude_piezo (float): Magnitude of the top peak for piezoelectric sensor data.
        top_index_psg (int): Index of the top peak for ground truth data.
        top_frequency_psg (float): Frequency of the top peak for ground truth data.
        top_magnitude_psg (float): Magnitude of the top peak for ground truth data.

    Returns:
        None (displays the plot)
    """
    # Calculate heart rate
    rate_piezo = int(top_frequency_piezo * 60)
    rate_psg = int(top_frequency_psg * 60)

    # Create subplots
    fig, axes = plt.subplots(2, 1, figsize=(8, 6), sharex=True)

    # Plot the FFT result for piezoelectric sensor data
    axes[0].plot(xf_piezo, np.abs(yf_piezo[0:N_piezo//2]),
                 color='teal', label='Piezo')
    axes[0].set_xlim(0, 2)
    axes[0].set_ylabel('Magnitude')

    # Mark the top peak on the plot for piezoelectric sensor data
    axes[0].scatter(xf_piezo[top_index_piezo], np.abs(
        yf_piezo[top_index_piezo]), color='orchid')

    # Annotate the top peak on the plot for piezoelectric sensor data
    annotation_x_piezo = xf_piezo[top_index_piezo] + 0.2
    annotation_y_piezo = np.abs(yf_piezo[top_index_piezo]) - 1
    axes[0].annotate(f"Freq: {top_frequency_piezo:.3f} Hz\nMagnitude: {top_magnitude_piezo:.2f}\nRate: {rate_piezo} beats/min",
                     xy=(xf_piezo[top_index_piezo], np.abs(
                         yf_piezo[top_index_piezo])),
                     xytext=(annotation_x_piezo, annotation_y_piezo),
                     arrowprops=dict(facecolor='black', arrowstyle='->'))
    axes[0].ticklabel_format(axis='y', style='sci', scilimits=(0, 0))
    axes[0].set_xlim(1, 5)
    axes[0].set_ylim(0, 30)

    # Plot the FFT result for ground truth data
    axes[1].plot(xf_psg, np.abs(yf_psg[0:N_psg//2]),
                 color='dodgerblue', label='PSG')
    axes[1].set_xlim(0, 2)
    axes[1].set_xlabel('Frequency')
    axes[1].set_ylabel('Magnitude')

    # Mark the top peak on the plot for ground truth data
    axes[1].scatter(xf_psg[top_index_psg], np.abs(
        yf_psg[top_index_psg]), color='teal')

    # Annotate the top peak on the plot for ground truth data
    annotation_x_psg = xf_psg[top_index_psg] + 0.2
    annotation_y_psg = np.abs(yf_psg[top_index_psg]) + 1
    axes[1].annotate(f"Freq: {top_frequency_psg:.3f} Hz\nMagnitude: {top_magnitude_psg:.2f}\nRate: {rate_psg} beats/min",
                     xy=(xf_psg[top_index_psg], np.abs(yf_psg[top_index_psg])),
                     xytext=(annotation_x_psg, annotation_y_psg),
                     arrowprops=dict(facecolor='black', arrowstyle='->'))
    axes[1].set_xlim(1, 5)
    axes[1].set_ylim(0, 10000000)
    axes[1].ticklabel_format(axis='y', style='sci', scilimits=(0, 0))

    # Add legends
    axes[0].legend()
    axes[1].legend()
    fig.text(0.5, -0.03, '(b)', fontsize=11, fontweight='bold', ha='center')

    # Display the plot
    plt.show()

# Example usage:
# plot_fft_heartbeat(xf_piezo, yf_piezo, xf_psg, yf_psg, N_piezo, N_psg, top_index_piezo, top_frequency_piezo, top_magnitude_piezo, top_index_psg, top_frequency_psg, top_magnitude_psg)


def plot_fft_breathing(xf_piezo, yf_piezo, xf_psg, yf_psg, N_piezo, N_psg, top_index_piezo, top_frequency_piezo, top_magnitude_piezo, top_index_psg, top_frequency_psg, top_magnitude_psg):
    """
    Function to plot the FFT result with peaks for piezoelectric sensor data and ground truth data.

    Parameters:
        xf_piezo (array): Frequencies for piezoelectric sensor FFT.
        yf_piezo (array): Magnitudes for piezoelectric sensor FFT.
        xf_psg (array): Frequencies for ground truth FFT.
        yf_psg (array): Magnitudes for ground truth FFT.
        N_piezo (int): Length of piezoelectric sensor data.
        N_psg (int): Length of ground truth data.
        top_index_piezo (int): Index of the top peak for piezoelectric sensor data.
        top_frequency_piezo (float): Frequency of the top peak for piezoelectric sensor data.
        top_magnitude_piezo (float): Magnitude of the top peak for piezoelectric sensor data.
        top_index_psg (int): Index of the top peak for ground truth data.
        top_frequency_psg (float): Frequency of the top peak for ground truth data.
        top_magnitude_psg (float): Magnitude of the top peak for ground truth data.

    Returns:
        None (displays the plot)
    """
    # Calculate respiratory rate
    rate_piezo = int(top_frequency_piezo * 60)
    rate_psg = int(top_frequency_psg * 60)

    # Create subplots
    fig, axes = plt.subplots(2, 1, figsize=(8, 6), sharex=True)

    # Plot the FFT result for piezoelectric sensor data
    axes[0].plot(xf_piezo, np.abs(yf_piezo[0:N_piezo//2]),
                 color='salmon', label='Piezo')
    axes[0].set_xlim(0, 2)
    axes[0].set_ylabel('Magnitude')

    # Mark the top peak on the plot for piezoelectric sensor data
    axes[0].scatter(xf_piezo[top_index_piezo], np.abs(
        yf_piezo[top_index_piezo]), color='orchid')
    axes[0].ticklabel_format(axis='y', style='sci', scilimits=(0, 0))

    # Annotate the top peak on the plot for piezoelectric sensor data
    annotation_x_piezo = xf_piezo[top_index_piezo] + 0.2
    annotation_y_piezo = np.abs(yf_piezo[top_index_piezo]) - 400
    axes[0].annotate(f"Freq: {top_frequency_piezo:.3f} Hz\nMagnitude: {top_magnitude_piezo:.2f}\nRate: {rate_piezo} breaths/min",
                     xy=(xf_piezo[top_index_piezo], np.abs(
                         yf_piezo[top_index_piezo])),
                     xytext=(annotation_x_piezo, annotation_y_piezo),
                     arrowprops=dict(facecolor='black', arrowstyle='->'))

    axes[0].set_xlim(1, 5)
    axes[0].set_ylim(0, 30)

    # Plot the FFT result for ground truth data
    axes[1].plot(xf_psg, np.abs(yf_psg[0:N_psg//2]),
                 color='dodgerblue', label='PSG')
    axes[1].set_xlim(0, 2)
    axes[1].set_xlabel('Frequency')
    axes[1].set_ylabel('Magnitude')

    # Mark the top peak on the plot for ground truth data
    axes[1].scatter(xf_psg[top_index_psg], np.abs(
        yf_psg[top_index_psg]), color='teal')
    axes[1].ticklabel_format(axis='y', style='sci', scilimits=(0, 0))

    # Annotate the top peak on the plot for ground truth data
    annotation_x_psg = xf_psg[top_index_psg] + 0.2
    annotation_y_psg = np.abs(yf_psg[top_index_psg]) - 150000
    axes[1].annotate(f"Freq: {top_frequency_psg:.3f} Hz\nMagnitude: {top_magnitude_psg:.2f}\nRate: {rate_psg} breaths/min",
                     xy=(xf_psg[top_index_psg], np.abs(yf_psg[top_index_psg])),
                     xytext=(annotation_x_psg, annotation_y_psg),
                     arrowprops=dict(facecolor='black', arrowstyle='->'))

    axes[1].set_xlim(1, 5)
    axes[1].set_ylim(0, 10000000)

    # Add legends
    axes[0].legend()
    axes[1].legend()

    # Display the plot
    fig.text(0.5, -0.03, '(a)', fontsize=11, fontweight='bold', ha='center')
    plt.show()

# Example usage:
# plot_fft_breathing(xf_piezo, yf_piezo, xf_psg, yf_psg, N_piezo, N_psg, top_index_piezo, top_frequency_piezo, top_magnitude_piezo, top_index_psg, top_frequency_psg, top_magnitude_psg)

import pickle
import numpy as np
import tensorflow as tf
from keras.models import Sequential
from keras.layers import Bidirectional, LSTM, Dense, Dropout, LayerNormalization, Masking
#from tensorflow.keras.layers import Input, Masking, LayerNormalization, Dropout, Dense, MultiHeadAttention, Conv1D, Flatten
from keras.callbacks import EarlyStopping

# File path to the pickle files (inputs and lables)
file_path_x = "./30sWindows/inputs_combined.pkl"
file_path_y = "./30sWindows/lables_combined.pkl"

# Open the data 
with open(file_path_x, 'rb') as pickle_file:
    inputs = pickle.load(pickle_file)

with open(file_path_y, 'rb') as pickle_file:
    lables = pickle.load(pickle_file)

# some (hyper)parameters
num_classes = 5
patience = 10
batch_size = 128

# Convert data to numpy array
inputs = np.array(inputs)
lables = np.array(lables)

# Get indices to shuffle
indices = np.arange(0,len(inputs))
np.random.shuffle(indices)

# Calculate lengths of splits based on proportions
total_length = len(indices)
train_length = int(total_length * 0.6)
valid_length = int(total_length * 0.2)
test_length = total_length - train_length - valid_length

# split the input 
x_train = inputs[indices[:train_length]]
x_valid = inputs[indices[train_length:train_length + valid_length]]
x_test = inputs[indices[train_length + valid_length:]]

# Convert the data to tf format
x_train = tf.Variable(x_train)
x_valid = tf.Variable(x_valid)
x_test = tf.Variable(x_test)

# Mask the input
x_train = tf.where(x_train!=0., x_train, 0.)
x_valid = tf.where(x_valid!=0., x_valid, 0.)
x_test = tf.where(x_test!=0., x_test, 0.)

# split the labels accordingly 
y_train = lables[indices[:train_length]]
y_valid = lables[indices[train_length:train_length + valid_length]]
y_test = lables[indices[train_length + valid_length:]]

# One-hot encoding of the labels
y_train_one_hot = tf.keras.utils.to_categorical(y_train, num_classes)
y_val_one_hot = tf.keras.utils.to_categorical(y_valid, num_classes)

# Define the Bidirectional LSTM model
model = Sequential()
model.add(Masking(mask_value=0., input_shape=(None, 109)))
model.add(Bidirectional(LSTM(128, return_sequences=True)))
model.add(LayerNormalization())
model.add(Dropout(0.3))
model.add(Bidirectional(LSTM(128, return_sequences=True)))
model.add(LayerNormalization())
model.add(Dropout(0.3))
model.add(Bidirectional(LSTM(128, return_sequences=True)))
model.add(Dropout(0.2))
model.add(Dense(128, activation='relu'))
model.add(Dropout(0.2))
model.add(Dense(num_classes, activation='softmax'))

# Compile the model
model.compile(optimizer='adam', loss='categorical_crossentropy', metrics=['accuracy'])
early_stopping = EarlyStopping(monitor='val_loss', patience=patience, restore_best_weights=True)

# Train the model
model.fit(x_train, y_train_one_hot, epochs=100, batch_size=batch_size, validation_data=(x_valid, y_val_one_hot), callbacks=[early_stopping])

# if you want to save the model, uncomment the following
#model.save("./model", save_format="h5")


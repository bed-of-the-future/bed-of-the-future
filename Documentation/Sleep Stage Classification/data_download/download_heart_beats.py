import sleepecg
from sleepecg.io.nsrr import  set_nsrr_token


# My personal access token
set_nsrr_token("22713-jqjUSxJxJBtydsc4mCMz")

# Output folder
output_folder = "./bed-of-the-future/Documentation/Sleep Stage Classification/Data/SHHS/HeartBeatExtracted"

# download, preprocess and save all preprocessed files, delete the .edfs after they are used
generator = sleepecg.read_shhs(records_pattern='*', heartbeats_source='ecg', offline=False, keep_edfs=False,
                                   data_dir=output_folder)
x = 0
for i in generator:
    print(x)
    x+=1



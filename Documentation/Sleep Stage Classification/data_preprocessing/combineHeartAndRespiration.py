import pickle  
import numpy as np 
import pandas as pd  

# File paths to processed heartbeats and respirations
file_path_x_breaths = "./30sWindows/inputs_breaths.pkl"  # Path to breath inputs pickle file
file_path_x_hearts = "./30sWindows/inputs_heart.pkl"  # Path to heart inputs pickle file
file_path_names_heart = "./30sWindows/recording_names_heart.pkl"  # Path to heart recording names pickle file
file_path_names_breath = "./30sWindows/recording_names_breaths.pkl"  # Path to breath recording names pickle 
file_path_labels = "./30sWindows/lables_heart.pkl"  # Path to heart labels pickle file

# Open the files
with open(file_path_labels, 'rb') as pickle_file:
    labels_heart = pickle.load(pickle_file)

with open(file_path_x_breaths, 'rb') as pickle_file:
    inputs_breath = pickle.load(pickle_file)

with open(file_path_x_hearts, 'rb') as pickle_file:
    inputs_heart = pickle.load(pickle_file)

with open(file_path_names_heart, 'rb') as pickle_file:
    names_heart = pickle.load(pickle_file)

with open(file_path_names_breath, 'rb') as pickle_file:
    names_breath = pickle.load(pickle_file)

# Read CSV files into pandas dataframes
df_shhs1 = pd.read_csv('./shhs1-dataset-0.20.0.csv', low_memory=False)  # Read CSV data for SHHS1
df_shhs2 = pd.read_csv('./shhs2-dataset-0.20.0.csv', encoding="cp1252", low_memory=False)  # Read CSV data for SHHS2

all_labels = []  # Initialize list to store combined labels
all_inputs_combined = []  # Initialize list to store combined inputs
counter = 0  # Counter for tracking progress
for name in range(len(names_heart)):
    index_breath = None
    for name2 in range(len(names_breath)):
        if names_heart[name] == names_breath[name2]:
            index_heart = name
            index_breath = name2
            break
    if index_breath is not None:
        all_labels.append(labels_heart[index_heart])  # Append heart labels to the list
        print(counter)  
        counter += 1 
        combined = []  # Initialize list for combined inputs

        # Convert breath inputs to numpy array
        inputs_breath[index_breath] = np.array(inputs_breath[index_breath])

        # Combine heart and breath inputs into a single list
        for sublist1, sublist2 in zip(inputs_heart[index_heart], inputs_breath[index_breath]):
            combined_sublist = []  # Initialize sublist for combined inputs
            combined_sublist.extend(sublist1)  # Extend sublist with heart inputs
            combined_sublist.extend(sublist2)  # Extend sublist with breath inputs
            combined.append(combined_sublist)  # Append combined sublist to the list
        all_inputs_combined.append(combined)  # Append combined list to the overall list

# Save combined inputs to a pickle file
with open("./30sWindows/inputs_combined.pkl", 'wb') as pickle_file:
    pickle.dump(all_inputs_combined, pickle_file)

# Save combined labels to a pickle file
with open("./30sWindows/labels_combined.pkl", 'wb') as pickle_file:
    pickle.dump(all_labels, pickle_file)


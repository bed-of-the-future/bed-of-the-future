import pandas as pd
import xml.etree.ElementTree as ET
import os
import pickle
import numpy as np

np.set_printoptions(suppress=True)

"""
HELPER FUNCTIONS
"""

def find_files_with_extension(folder_path, file_extension):
    """
    Given a folder path and a file extension, this function searches recursively within the folder
    for files with the specified extension and returns their paths.

    Parameters:
    folder_path (str): The path to the folder to search within.
    file_extension (str): The file extension to search for.

    Returns:
    list: A list of file paths that match the given file extension.

    Example:
    folder_path = '/path/to/folder'
    file_extension = '.txt'
    find_files_with_extension(folder_path, file_extension)
    # Output: ['/path/to/folder/file1.txt', '/path/to/folder/subfolder/file2.txt']
    # Explanation: The function returns a list of file paths with the '.txt' extension
    # found within the specified folder and its subfolders.
    """

    file_paths = []
    for root, dirs, files in os.walk(folder_path):
        for file in files:
            if file.endswith(file_extension):
                file_paths.append(os.path.join(root, file))
    return file_paths

"""
SLEEP STAGE ANNOTATION PROCESSING
"""

def extract_sleep_stages(xml_file):
    """
    Extracts sleep stages and durations from an XML file containing sleep stage annotations.

    Parameters:
    xml_file (str): The path to the XML file containing sleep stage annotations.

    Returns:
    list: A list of sleep stages, with one stage annotation per 30-second time window.

    Example:
    xml_file = 'sleep_annotations.xml'
    extract_sleep_stages(xml_file)
    # Output: [0, 0, 0, 1, 1, 1, 2, 2, ...]
    # Explanation: Sleep stage annotations extracted from the XML file.
    """

    tree = ET.parse(xml_file)
    root = tree.getroot()

    # extract the sleep stages from the xml file
    stages_list = []
    # the xml saves all data in so-called scoredEvents
    for event in root.findall('.//ScoredEvent'):
        event_type_elem = event.find('EventType')
        if event_type_elem.text is not None:
            event_type = event_type_elem.text

            # The sleep stages are saved as the scored event "stages"
            if 'Stages' in event_type:
                # for each sleep stage annotation, the sleep stage  and the duration are extracted
                stage = int(event.find('EventConcept').text.split('|')[-1])
                duration = int(float(event.find('Duration').text))
                # save one marker, i.e. 0,1,2,3 per every 30sec, i.e. one annotation per 30s time window
                n_stages = duration / 30
                stages_list.append([stage] * int(n_stages))

    return [item for sublist in stages_list for item in sublist]


def check_sleep_stage_annotation(sleep_stage_list):
    """
    Checks whether a given list of sleep stage annotations is valid according to predefined rules.
    This is necessary, because some annotations cannot be used, for example if only wake/sleep is annotated.
    Additionally, all records where a 30s window is annotated with 9(=undefined) are excluded.

    Parameters:
    sleep_stage_list (list): A list of integers representing sleep stage annotations.

    Returns:
    bool: True if the sleep stage annotations are valid, False otherwise.

    Example:
    sleep_stage_list = [0, 1, 2, 3, 4, 5]
    check_sleep_stage_annotation(sleep_stage_list)
    # Output: True
    # Explanation: All sleep stage annotations (0, 1, 2, 3, 4, 5) are valid according to predefined rules.

    sleep_stage_list = [0, 1, 2, 4]
    check_sleep_stage_annotation(sleep_stage_list)
    # Output: False
    # Explanation: The sleep stage annotation (0, 1, 2, 4) are not valid according to predefined rules (In this case,
      because "5" = REM sleep, is missing).
    """

    allowed_sleep_stage_annotations = [(0, 1, 2, 3, 4, 5), (0, 2, 3, 5), (0, 1, 2, 4, 5),
                                       (0, 1, 2, 3, 5), (0, 2, 3, 4, 5)]
    unique_values = tuple(np.unique(np.array(sleep_stage_list)))
    if unique_values in allowed_sleep_stage_annotations:
        return True
    else:
        return False


def colapse_sleep_stages(matrix):
    """
    Collapses sleep stages according to predefined rules.
    We simplified the sleep stages into:

    0 = wake
    1 = light sleep
    2 = deep sleep
    3 = REM sleep

    Parameters:
    matrix (list of lists): A matrix representing sleep stages.

    Returns:
    list of lists: A matrix with collapsed sleep stages.

    Example:
    matrix = [[1, 2, 3], [4, 5, 0]]
    colapse_sleep_stages(matrix)
    # Output: [[1, 1, 2], [2, 3, 0]]
    # Explanation: Sleep stages collapsed according to predefined rules.
    """

    for i in range(len(matrix)):
        for j in range(len(matrix[i])):
            if matrix[i][j] == 2:
                matrix[i][j] = 1
            elif matrix[i][j] == 3 or matrix[i][j] == 4:
                matrix[i][j] = 2
            elif matrix[i][j] == 5:
                matrix[i][j] = 3
    return matrix


"""
HEART BEAT PROCESSING
"""

def split_array_in_30s_windows(arr):
    """
        Splits an array of heartbeats into 30-second windows and handles error conditions.

        Parameters:
        arr (array): An array containing timestamps of heartbeats.

        Returns:
        list: A list of lists, where each sublist represents heartbeats within a 30-second window.

        Example:
        arr = [0, 10, 20, 35, 45, 60, 70]
        split_array_in_30s_windows(arr)
        # Output: [[10, 20], [35, 45, 60], [70]]

        """
    # calculate the number and range of each 30s time window
    thirties = np.arange(0, arr[-1], 30)
    window_list = []

    # split the list of heart beats into 30s windows
    for entry in range(len(thirties) - 1):
        #start time of current time window
        start = thirties[entry]
        #end time of current time window
        end = thirties[entry + 1]

        # compute the heartbeats within the current 30s window
        mask = (arr >= start) & (arr <= end)
        split_indices = np.where(mask[1:] != mask[:-1])[0] + 1

        # handle case of the first 30s window, i.e. insert a zero at the beginning
        if len(split_indices) == 1:
            split_indices = np.insert(split_indices, 0, 0)

        # exclude cases where there is no heart beat with hin the current time window (errors in the meassurement)
        if len(split_indices) != 0:
            beats_to_append = list(arr[split_indices[0]:split_indices[1]])

            # exclude cases where the heartbeat is above 180bpm or below 20
            if len(beats_to_append) > 90 or len(beats_to_append) < 10:
                window_list.append([0])
            else:
                window_list.append(beats_to_append)
        else:
            # mask for errorous records 0.
            window_list.append([0.])

    return window_list


def find_zero_subsublists_indices(three_d_list):
    """
    Given a 3D list, this function identifies and returns the indices of the subsublists
    (nested lists within the nested lists) that contain only zeros.

    Parameters:
    three_d_list (list): A 3D list containing sublists, each containing subsublists.

    Returns:
    list: A list of tuples representing the indices (i, j) of the subsublists that contain only zeros.

    Example:
    three_d_list = [[[0, 0, 0], [1, 0, 0]], [[0, 0, 0], [0, 0, 0]]]
    find_zero_subsublists_indices(three_d_list)
    # Output: [(0, 0), (1, 0)]
    # Explanation: The subsublists at indices (0, 0) and (1, 0) contain only zeros.
    """

    zero_indices = []
    for i, sublist in enumerate(three_d_list):
        for j, subsublist in enumerate(sublist):
            # Check if all elements in the subsublist are zeros
            if all(element == 0 for element in subsublist):
                zero_indices.append((i, j))

    return zero_indices


def calculate_differences_between_beats_np(seconds_arr):
    """
    Calculates the time differences between consecutive beats in an array.

    Parameters:
    seconds_arr (array): An array containing timestamps of heartbeats.

    Returns:
    list: A list of rounded time differences between consecutive beats.

    Example:
    seconds_arr = [0, 1.2, 3.5, 5.8]
    calculate_differences_between_beats_np(seconds_arr)
    # Output: [1.2, 2.3, 2.3]
    # Explanation: Time differences between consecutive beats in seconds_arr.
    """

    differences = np.diff(seconds_arr)
    return np.round(differences, 3).tolist()


def pad_subsublists(three_d_list, target_length=90):
    """
    Pads subsublists in a 3D list with 0 to make their lengths equal to the target length.

    Parameters:
    three_d_list (list): A 3D list containing sublists, each containing subsublists.
    target_length (int): The desired length of each subsublist after padding.

    Example:
    three_d_list = [[[1.2, 2.3], [4.5]], [[3.2, 5.7, 8.9], [1.1, 2.2, 3.3, 4.4]]]
    pad_subsublists(three_d_list, target_length=5)
    # Output: three_d_list is modified in-place with padded subsublists.
    """

    for sublist in three_d_list:
        for subsublist in sublist:
            differences = calculate_differences_between_beats_np(subsublist)
            while len(differences) < target_length:
                differences.append(0)
            subsublist.clear()  # Clear existing elements
            subsublist.extend(differences)  # Pad with differences

"""
Filter the Records based on SHHS ratings
"""
# SHHS1
# Read the CSV file containing SHHS1 data
df = pd.read_csv('./Data/shhs1-dataset-0.20.0.csv', low_memory=False)
# Filter rows based on specified conditions
filtered_df_shhs1 = df[
   # (df['overall_shhs1'] >= 4) &
    (df['hrqual'] == 4) &
    (df['abdoqual'] == 4) &
    (df['abdodur'] >= 4)
]
# Extract 'nsrrid' values
nsrrid_values = filtered_df_shhs1['nsrrid'].tolist()
all_records_shhs1 = ["shhs1-" + str(element) for element in nsrrid_values]

# SHHS2
# Read the CSV file containing SHHS2 data
df = pd.read_csv('./Data/shhs2-dataset-0.20.0.csv', encoding="cp1252", low_memory=False)

# Filter rows based on conditions
# Excluding airflow quality for now due to limited records
filtered_df_shhs2 = df[
    #(df['overall_shhs2'] >= 4) &
    (df['quhr'] == 5) &
    (df['quabdo'] == 5) &
    (df['abdodur'] >= 4)
]
# Extract 'nsrrid' values
nsrrid_values2 = filtered_df_shhs2['nsrrid'].tolist()
all_records_shhs2 = ["shhs2-" + str(element) for element in nsrrid_values]

# Combine all record names in one list
all_records = all_records_shhs1 + all_records_shhs2

"""
Read in the downloaded records. The XML files contain the sleep stages, the NPY files contain the heart beats.
"""

# Paths to the folders containing XML and NPY files
xml_folder_path = './Data/reformattedData/shhs_from_sleepECG/polysomnography/annotations-events-nsrr/'
preprocessed_edfs_path = './Data/reformattedData/shhs_from_sleepECG/preprocessed/heartbeats'

# Get paths to all individual records
all_xml_paths = find_files_with_extension(xml_folder_path, "xml")
all_edf_paths = find_files_with_extension(preprocessed_edfs_path, "npy")

inputs = []
labels = []

# Counter to count the number of processed records
counter = 0
recording_names = []
for record in all_records:
    print(counter)
    counter += 1
    # For the current record, get the respective NPY and XML paths
    for xml in all_xml_paths:
        current_xml = []
        if record in xml:
            current_xml = xml
            break

    for npy in all_edf_paths:
        current_npy = []
        if record in npy:
            test_npy = npy
            current_npy = np.load(npy)
            break

    # Check that the XML and the NPY files belong to the same recording
    if len(current_npy) != 0 and len(current_xml) != 0:

        # Extract sleep stages from XML
        sleep_stages = extract_sleep_stages(current_xml)
        if check_sleep_stage_annotation(sleep_stages):
            pass
        else:
            continue

        # Extract heart beats from NPY
        heart_beats = split_array_in_30s_windows(current_npy)

        # Put everything together
        sleep_stages.pop()  # Remove last element to match lengths
        inputs.append(heart_beats)
        labels.append(sleep_stages)
        recording_names.append(record)

# get the max length of the 30s windows and of the entire records
# required to pad the shorter ones
lengths_records = []
for i in inputs:
    lengths_records.append(len(i))

lengths_30s_windows = []
for i in inputs:
    for j in i:
        lengths_30s_windows.append(len(j))

max_record_len = max(lengths_records)
max_30s_length = max(lengths_30s_windows)


# Collapse sleep stages
sleep_stages = colapse_sleep_stages(labels)

# All 30s windows that contain fewer entries are padded with zeros
pad_subsublists(inputs, max_30s_length)

# Delete records where the length of the sleep stage annotation and the length of the heart beat records do not match
wrong_lengths = []
for i in range(len(labels)):
    if len(labels[i]) != len(inputs[i]):
        wrong_lengths.append(i)
inputs = [value for index, value in enumerate(inputs) if index not in list(wrong_lengths)]
labels = [value for index, value in enumerate(labels) if index not in list(wrong_lengths)]
recording_names = [value for index, value in enumerate(recording_names) if index not in list(wrong_lengths)]

# Pad all labels with "4" to make them the same length
desired_length = max_record_len
labels_padded = [sublist + [4] * (desired_length - len(sublist)) for sublist in labels]

# Pad the 30s Windows so that all have the same length (max 30s window)
zero_sublist = [0.] * max_30s_length  
inputs_padded = [sublist + [zero_sublist] * (desired_length - len(sublist)) for sublist in inputs]

os.makedirs("./30sWindows", exist_ok=True)

# File paths to save the pickle files
file_path_inputs_padded = "./30sWindows/inputs_heart.pkl"
file_path_lables_padded = "./30sWindows/lables_heart.pkl"
file_path_names = "./30sWindows/recording_names_heart.pkl"

# Write the lists to files
with open(file_path_inputs_padded, 'wb') as pickle_file:
    pickle.dump(inputs_padded, pickle_file)

with open(file_path_lables_padded, 'wb') as pickle_file:
    pickle.dump(labels_padded, pickle_file)

with open(file_path_names, 'wb') as pickle_file:
    pickle.dump(recording_names, pickle_file)

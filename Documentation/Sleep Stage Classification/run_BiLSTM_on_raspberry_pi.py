from keras.models import load_model
from datetime import datetime, timedelta
import numpy as np
import influxdb_client
from influxdb_client import Point
from influxdb_client.client.write_api import SYNCHRONOUS


def timestamps_until_threshold(timestamps, thrs=1200):
    """
    Retrieves timestamps from a list until the difference between consecutive timestamps exceeds a threshold.

    Parameters:
    timestamps (list): A list of timestamps.
    thrs (int): The threshold value in seconds. Default is 1200 seconds (20 minutes).

    Returns:
    list: A list of timestamps satisfying the threshold condition.

    """

    result = []
    for i in range(len(timestamps) - 1, -1, -1):  # Include the first timestamp as well
        if i < len(timestamps) - 1:
            difference = (timestamps[i + 1] - timestamps[i]).total_seconds()
            if difference > thrs:
                break
        result.append(timestamps[i])
    result.reverse()  # Reverse the list to get the timestamps in correct order
    return result


def generate_timestamps(original_timestamp, num_timestamps):
    """
    Generates a list of timestamps at regular intervals from a given starting timestamp.

    Parameters:
    original_timestamp (float): The starting timestamp in Unix format.
    num_timestamps (int): The number of timestamps to generate.

    Returns:
    list: A list of generated timestamps.

    """

    # Convert original timestamp to datetime object
    original_datetime = datetime.utcfromtimestamp(original_timestamp)

    # Initialize a list to store the timestamps
    timestamps = []

    # Iterate to generate timestamps
    for i in range(num_timestamps):
        # Calculate the delta in seconds (multiples of 30)
        delta_seconds = (i + 1) * 30

        # Add the delta to the original datetime
        new_datetime = original_datetime + timedelta(seconds=delta_seconds)

        # Convert the new datetime to Unix timestamp and append to the list
        new_timestamp = new_datetime.timestamp()
        timestamps.append(new_timestamp)

    return timestamps


def split_array_in_30s_windows(arr):
    """
    Splits an array of heartbeats into 30-second windows and handles error conditions.

    Parameters:
    arr (array): An array containing timestamps of heartbeats.

    Returns:
    list: A list of lists, where each sublist represents heartbeats within a 30-second window.

    Example:
    arr = [0, 10, 20, 35, 45, 60, 70]
    split_array_in_30s_windows(arr)
    # Output: [[10, 20], [35, 45, 60], [70]]

    """

    # Calculate the number and range of each 30s time window
    thirties = np.arange(0, arr[-1], 30)
    window_list = []
    # Split the list of heartbeats into 30s windows
    for entry in range(len(thirties) - 1):
        # Start time of current time window
        start = thirties[entry]
        # End time of current time window
        end = thirties[entry + 1]

        # Compute the heartbeats within the current 30s window
        mask = (arr >= start) & (arr <= end)
        split_indices = np.where(mask[1:] != mask[:-1])[0] + 1

        # Handle the case of the first 30s window, i.e., insert a zero at the beginning
        if len(split_indices) == 1:
            split_indices = np.insert(split_indices, 0, 0)

        # Exclude cases where there is no heartbeat within the current time window (errors in the measurement)
        if len(split_indices) != 0:
            beats_to_append = list(arr[split_indices[0]:split_indices[1]])

            window_list.append(beats_to_append)
        else:
            # Mask for erroneous records 0.
            window_list.append([0.])

    return window_list


# RETRIEVE DATA FROM INFLUXDB

# Influx Login
url = "http://localhost:8086"
token = 'rqU6HxUnS6RrYfthKGjBdIqkbe9Us8DdJSnm6K8IIz5mm74I8FBKtD25uhTiLAAfafyOBg8agC3sqsmSRyKUeQ=='
org = "smartbed"
bucket = "hypno"

# Set up client and APIs
client = influxdb_client.InfluxDBClient(url=url, token=token, org=org)

# Initialize the InfluxDB client
query_api = client.query_api()

# Query to get the heartbeats and respirations of the last 24h
query = '''
    from(bucket: "Data")
      |> range(start: -24h)
      |> filter(fn: (r) => r["_measurement"] == "piezo")
      |> filter(fn: (r) => r["_field"] == "Breath" or r["_field"] == "HeartBeat")
'''

# Execute query
tables = client.query_api().query(query, org=org)

# Lists to store timestamps
breath_timestamps = []
heartbeat_timestamps = []

# Parse query results
for table in tables:
    for row in table.records:
        if row.get_field() == "Breath":
            breath_timestamps.append(row.get_time())
        elif row.get_field() == "HeartBeat":
            heartbeat_timestamps.append(row.get_time())

"""
Retrieve the last connected records starting from the latest data point, 
return all data points until the difference between two consecutive ones is larger than the threshold. 
Default value for the threshold: 1200sec.
"""

selected_breaths = timestamps_until_threshold(breath_timestamps)
selected_heartbeats = timestamps_until_threshold(heartbeat_timestamps)

# Convert the timestamps from datetime to Unix time
time_values_breath = [dt.timestamp() for dt in selected_breaths]
time_values_heart = [dt.timestamp() for dt in selected_heartbeats]

# Subtract the first timestamp from all the others, such that the values start at zero
time_values_breath = [value - time_values_breath[0] for value in time_values_breath]
time_values_heart = [value - time_values_heart[0] for value in time_values_heart]

# Split the recording into 30s windows. 30s is the standard value used for sleep stage annotation
heart_30s_windows = split_array_in_30s_windows(time_values_heart)
breath_30s_windows = split_array_in_30s_windows(time_values_breath)
max_len = max(len(heart_30s_windows), len(breath_30s_windows))

# Pad the data, such that the BiLSTM can handle it
inputs_heart = []
for sublist in heart_30s_windows:
    differences = np.diff(sublist)
    differences = np.round(differences, 3).tolist()
    while len(differences) < 90:
        differences.append(0.)
    if len(differences) > 90:
        differences = differences[0:90]
    inputs_heart.append(differences)

inputs_breath = []
for sublist in breath_30s_windows:
    differences = np.diff(sublist)
    differences = np.round(differences, 3).tolist()
    while len(differences) < 19:
        differences.append(0.)
    if len(differences) > 19:
        differences = differences[0:19]
    inputs_breath.append(differences)

# Combine heartbeats and respiration into a single feature vector
combined_list = []
for sublist1, sublist2 in zip(inputs_heart, inputs_breath):
    combined_sublist = []
    combined_sublist.extend(sublist1)
    combined_sublist.extend(sublist2)
    combined_list.append(combined_sublist)

# Convert to numpy and reshape for use with the BiLSTM
# (The BiLSTM was trained on batches of data, therefore, the "batch dimension" must be added if we use it on a single input)
inputs_final = np.array(combined_list)
inputs_final = inputs_final.reshape(-1, max_len, 109)

# Provide the path to the trained model
model_path = '/home/raspberry/Documents/neuralNetwork/model_trained.h5'

# Load the model
model = load_model(model_path)

# Forward pass, infer the sleep stages from the data
predictions = model.predict(inputs_final)

# The output is a distribution over all possible classes,
# take the class with the highest probability for each 30s window
argmax_lowest_sublists = np.argmax(predictions, axis=2)
prediction = argmax_lowest_sublists[0]

# Given the start of the recording, compute a timestamp for each sleep stage,
# i.e., we computed a sleep stage for every 30s window, now we need to define their corresponding real-time
timestamps = generate_timestamps(selected_breaths[0].timestamp(), len(prediction))
time_and_stage = [[datetime.utcfromtimestamp(timestamps[i]), prediction[i]] for i in
                  range(min(len(timestamps), len(prediction)))]

# Write data to InfluxDB
write_api = client.write_api()
for timestamp, value in time_and_stage:
    point = Point("measurement_name").time(timestamp).field("value", value)
    write_api.write(bucket="hypno", org=org, record=point, write_options=SYNCHRONOUS)

# Sleep Stage Classification

This repo contains all data and code to reproduce the BiLSTM used in the smart bed to infer the sleep stages from vital signs (heartbeat & respiration) recording.

## Data
The data is from the Sleep Heart Health Study (SHHS). It contains 356 GB of PSG recordings with sleep scoring annotations performed by experts. 

### Download and extraction
In the folder data_download, we provide two scripts to download the data from the SHHS website. An access token is required to do so, which can be applied for [here](https://sleepdata.org/data/requests/shhs/start). The script [download_heart_beats.py](https://gitlab.gwdg.de/bed-of-the-future/bed-of-the-future/-/blob/main/Documentation/Sleep%20Stage%20Classification/data_download/download_heart_beats.py?ref_type=heads) downloads the individual records from the SHHS database using a generator. It immediatly extracts the ECG signal and saves the result as a numpy file in a specified folder. It is possible to keep the entire recording (as a .edf file), but due to memory usage the default option is not to. The script download_respiration.py does the same, but for the respiratory signal.

### Preprocessing
All scripts for the preprocessing can be found in this [folder](https://gitlab.gwdg.de/bed-of-the-future/bed-of-the-future/-/tree/main/Documentation/Sleep%20Stage%20Classification/data_preprocessing?ref_type=heads). The preprocessing pipeline consists of three steps: 
1. execute the [heartbeat preprocessing script](https://gitlab.gwdg.de/bed-of-the-future/bed-of-the-future/-/blob/main/Documentation/Sleep%20Stage%20Classification/data_preprocessing/preprocessHeartBeatData.py?ref_type=heads)
2. execute the [respiration preprocessing script](https://gitlab.gwdg.de/bed-of-the-future/bed-of-the-future/-/blob/main/Documentation/Sleep%20Stage%20Classification/data_preprocessing/preprocessRespiratoryData.py?ref_type=heads)
3. execute the [combining script](https://gitlab.gwdg.de/bed-of-the-future/bed-of-the-future/-/blob/main/Documentation/Sleep%20Stage%20Classification/data_preprocessing/combineHeartAndRespiration.py?ref_type=heads)

##### Cleaning
We cleaned the data using quality assessments provided by the SHHS database. We only included records up to a certain threshold based on their "_overall quality_" rating, "_quality of the heart rate signal_" rating,  "_quality of the abdominal signal_" and the "_hours of usable signal_" rating. We also excluded records that had an unfitting sleep stage annotation (for example, only wake<->sleep distinction). After cleaning, we obtained 3548 whole night recordings.  

##### Sleep Stages
We decided to use less sleep stages as proposed by Rechtschaffen & Kales, 1968. We combined sleep stages N1 and N2 into a single sleep stage called "_light sleep_" and we combined stages N3 and N4 into a single stage called "_deep sleep_". The awake and REM sleep stages stayed as they were.

##### Padding
We used zero padding to align the dimensions of the input data. Consequently, we used masking to inform the model about the padded sequences.

##### Format
The result will be saved in the folder [30sWindows](https://gitlab.gwdg.de/bed-of-the-future/bed-of-the-future/-/tree/main/Documentation/Sleep%20Stage%20Classification/Data/30sWindows?ref_type=heads) as two .pickel files. One contains the input data, i.e. a 3D list with the shape `[recording,30sWindows,values]`, the other the lables as a 2D list, with the shape `[recording,values]` (the difference in the shape is because each 30s window contains multiple values (the vitalsigns whitin that window), but is mapped onto one single value (the sleep stage)). 


## Model and Training
We used a Bi-Directional Long-Short Term Memory Neural Network (BiLSTM). The network maps the input sequence to the output sequence (sequence-to-sequence). Although being trained on longer sequences (4.6H - 11H), it can be used for shorter sequnces as well (we did not assess the performance of the model for such cases, but since they are not in the training data, i.e. out of domain, we expect it to perform worse). We trained the model on a 0.6/0.2/0.2 split of training, validation and test data, respectively. 

We trained the model on a Nvidia RTX A5000.

For the details of the model, the training and the other (hyper)parameter, see [here](https://gitlab.gwdg.de/bed-of-the-future/bed-of-the-future/-/blob/main/Documentation/Sleep%20Stage%20Classification/BiLSTM.py?ref_type=heads)

## Results
We obtained a accuracy of ~75%, and a (weighted) F1 score of (0.745) 0.710. 

![Confusion Matrix](/Documentation/Sleep Stage Classification/images/ConfusionMatrix.png)

![Normalized Confusion Matrix](/Documentation/Sleep Stage Classification/images/NormalizedConfusionMatrix.png)



import sys
import whisper
import pyaudio
import wave
from pydub import AudioSegment
from datetime import datetime
import os


def check_for_stop_signal():
    return os.path.exists("stop_signal.txt")

def record_audio(duration, file_name):

    try:
        os.remove("stop_signal.txt")
    except FileNotFoundError:
        pass

    audio_format = pyaudio.paInt16
    channels = 2
    sample_rate = 44100
    recording_duration = duration
    p = pyaudio.PyAudio()

    stream = p.open(format=audio_format,
                    channels=channels,
                    rate=sample_rate,
                    input=True,
                    frames_per_buffer=1024)

   # print("Recording started.")
    frames = []

    for i in range(0, int(sample_rate / 1024 * recording_duration)):
        if check_for_stop_signal():
            #print("Stop signal received. Ending recording.")
            break
        data = stream.read(1024)
        frames.append(data)

    #print("Recording completed.")

    stream.stop_stream()
    stream.close()
    p.terminate()

    wf = wave.open(file_name + ".wav", 'wb')
    wf.setnchannels(channels)
    wf.setsampwidth(pyaudio.PyAudio().get_sample_size(audio_format))
    wf.setframerate(sample_rate)
    wf.writeframes(b''.join(frames))
    wf.close()

def convert_to_mp3(file_name):
    sound = AudioSegment.from_wav(file_name + ".wav")

    sound.export(file_name + ".mp3", format="mp3")
    #print("Conversion to MP3 completed.")

if __name__ == "__main__":
    if len(sys.argv) != 3:
        #print("Usage: python script.py <recording_duration> <output_filename>")
        sys.exit(1)

    recording_duration = int(sys.argv[1])
    file_name = sys.argv[2]

    # Add current date to the "Output.txt" file
    current_date = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
    with open("Output.txt", 'a') as file:
        file.write(f"\n\nDiary Entry: {current_date}\n")

    record_audio(recording_duration, file_name)
    convert_to_mp3(file_name)

    model = whisper.load_model("small")
    result = model.transcribe(f"{file_name}.mp3")
    print(result["text"])



    # Append the recording to the "Output.txt" file
    with open("Output.txt", 'a') as file:
        file.write(result["text"] + '\n')
        #print("Transcription added to 'Output.txt' file.")

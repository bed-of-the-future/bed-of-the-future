**Prerequisites:**
The Dream Diary code needs several packages in order to work. Creating a virtual environment is necessary, conaining the sys; whisper; pyaudio; wave; pydub; datetime; os packages.

**Implementation and Activation**
In order to let the Dream Diary run in Node-Red, one has to navigate to the virtual environment on the console:
- cd {source to your venv}/bin
Then, activate the virtual environment:
- source activate
After activating the venv, one can navigate out of the venv folder. Then, open Node-Red on your console, while the venv is activated (simply by typing "Node-Red"). 

The Dream Diary is now being able to run via the interface, starting and stopping a recording via the microphone symbole (button). Depending on the computational recourses of your Raspberry Pi, it may take up to several minutes until the speech-to-text translation is finished. So try to keep your dreams as short as possible. 